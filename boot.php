<?php
declare(strict_types=1);
/**
 * Helper elements that just did not wanna fit anywhere.
 *
 * Note: In addition to being a placeholder for simple elements this file serves us as wrapper for all other 
 * requirements plugin needs at the begining of a lifecycle.
 * 
 */
namespace RefineIt\Support;

require_once __DIR__ . '/Plugin/InfoInterface.php';
require_once __DIR__ . '/../Info.php';
require_once __DIR__ .'/Autoloader.php';

use RefineIt\Info;


/**
 * Activation
 *
 * Main activation function triggered by click on 'Activate' link in WP installation.
 * 
 * @return void
 * 
 */
function activation(): void {
	$activation_hook = Info::g()['mu_prefix_module'] . 'module_activation';
	\do_action($activation_hook);
}


/**
 * Deactivation
 *
 * Main deactivation function triggered by click on 'Deactivate' link in WP installation.
 * 
 * @return void
 * 
 */
function deactivation(): void {
	$deactivation_hook = Info::g()['mu_prefix_module'] . 'module_deactivation';
	\do_action($deactivation_hook);
}
