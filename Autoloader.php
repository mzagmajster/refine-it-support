<?php

declare(strict_types=1);

namespace RefineIt\Support;

use RefineIt\Info;


class Autoloader {
	/**
	 * Paths to check.
	 * 
	 * @var array
	 */
	private $search_paths;

	/**
	 * Buičds $search_paths array.
	 * 
	 * @param  string $root 
	 * @return array        List of paths to search for language constructs.
	 */
	private function get_paths(string $root): array {
		$this->search_paths[] = $root;
		$paths = scandir($root);


		foreach ($paths as $p) {
			if($p == '.' || $p == '..') {
				continue;
			}

			$real_path = $root . '/' . $p;
			if(!is_dir($real_path)) {
				continue;
			}

			$this->search_paths[] = $real_path;
			$this->get_paths($real_path);
		}

		/*echo "<pre>";
		print_r($this->search_paths);
		echo "</pre>";*/
		return $this->search_paths;
	}

	/**
	 * Checks if folder should be included in autoload directory scanning.
	 * 
	 * @param  string  $folder Folder in question.
	 * @return boolean         If folder should be included true, false otherwise.
	 */
	private function is_autoload_required(string $folder): bool {
		$first_letter = $folder[0];
		if(!\IntlChar::isupper($first_letter)) {
			return false;
		}

		return true;
	}

	/**
	 * Constructor.
	 * 
	 * @param string Name of the module.
	 */
	public function __construct(string $root_folder) {
		$this->search_paths = array();
		
		// Root path.
		$this->search_paths[] = Info::root_plugin_path() . $root_folder;

		// Add required folders.
		// Because we need the entire configuration just of the structure we load config again into variable.
		// TODO: Improve (optimize).
		$required_folders = Info::get('structure.php');
		foreach ($required_folders as $folder_properties) {
			$path = $this->search_paths[0] . '/' . $folder_properties['name'];

			// @todo: Trigger some error if required folder is missing.
			if($this->is_autoload_required($folder_properties['name']) && is_dir($path)) {
				$this->get_paths($path);
			}
		}

		spl_autoload_register(array($this, 'load_element'));
	}

	/**
	 * Load specific element,
	 * 
	 * @param  string $class_name Class name, interface, etc.
	 * @return void               [description]
	 */
	public function load_element(string $class_name): void {

		$resolution = explode('\\', $class_name);
		$real_class_name = $resolution[count($resolution) - 1];

		$file_path = '';
		$i = 0;
		$path_count = count($this->search_paths);

		while ($i < $path_count && !file_exists($file_path)) {
			$file_path = $this->search_paths[$i] . '/' . $real_class_name . '.php';
			// echo "$file_path<br>";
			// echo "next path to check $file_path<br>";
			$i++;
		}

		/*if($i == $path_count) {
			return;
		}*/

		if (!file_exists($file_path)) {
			return;
		}

		// echo "including class $class_name ....<br>";
		include_once $file_path;
	}
}