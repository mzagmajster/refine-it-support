<?php

declare(strict_types=1);

namespace RefineIt\Support\Plugin;

interface ContextInterface {
	/**
	 * This method must offer access to appropriate config object for the given scope.
	 * 
	 * @return array
	 */
	function g(): array;
}