<?php

declare(strict_types=1);

namespace RefineIt\Support\Plugin;


interface InfoInterface {
	/**
	 * Must return absolute path with trailing slash to root folder for appropricate module.
	 * 
	 * @return string
	 */
	static function root_path(): string;

	/**
	 * Must return absolute url with trailing slash for appropriate module.
	 * 
	 * @return string
	 */
	static function root_url(): string;
}
