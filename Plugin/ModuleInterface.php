<?php

declare(strict_types=1);

namespace RefineIt\Support\Plugin;


interface ModuleInterface {

	/**
	 * Must initialize all components to provide functionality.
	 * 
	 * @return void
	 */
	function run_plugin(): void;

	/**
	 * Must implement behavior on plugin activation.
	 * 
	 * @return void
	 */
	function activation(): void;

	/**
	 * Must implement behavior on plugin deactivation.
	 * 
	 * @return void
	 */
	function deactivation(): void;

	/**
	 * Must register any possible module assets of group: scripts.
	 * 
	 * @return void
	 */
	function register_scripts(): void;

	/**
	 * Must register any possible module assets of group: styles.
	 * 
	 * @return void
	 */
	function register_styles(): void;
}