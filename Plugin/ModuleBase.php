<?php

declare(strict_types=1);

namespace RefineIt\Support\Plugin;

use RefineIt\Support\Plugin\ModuleInterface;
use RefineIt\Support\Plugin\Context;
use RefineIt\Support\Config;


abstract class ModuleBase implements ModuleInterface, ContextInterface {
	/**
	 * Construct and initialize object.
	 */
	public function __construct() {
		$text_domain = $this->g()['mu_text_domain'];

		if (strlen($text_domain)) {
			$languages_path = \RefineIt\Info::root_path() . '/' . \RefineIt\Info::g()['assets_folder']['name'];
			load_plugin_textdomain($text_domain, false, $languages_path);
		}

		add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_styles' ) );

		// Note: We need plugin prefix from child context (derived class) so we use g method.
		$module_prefix = $this->g()['mu_prefix_module'];
		add_action(($module_prefix . 'module_activation'), array($this, 'activation'), 10, 0);
		add_action(($module_prefix . 'module_deactivation'), array($this, 'deactivation'), 10, 0);

		$this->run_plugin();
	}

	abstract public function g(): array;

	abstract public function activation(): void;

	abstract public function deactivation(): void;

	abstract public function register_scripts(): void;

	abstract public function register_styles(): void;

	abstract public function run_plugin(): void;
}
