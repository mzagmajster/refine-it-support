<?php

declare(strict_types=1);

namespace RefineIt\Support\Plugin;

abstract class ResourceBase {
	/**
	 * Absolute path to resource folder in question.
	 * 
	 * @var string
	 */
	protected $root_path;

	/**
	 * Absolute URL to resource folder in question.
	 * 
	 * @var string
	 */
	protected $root_url;

	/**
	 * Constructor.
	 * 
	 * @param string $root_path Absolute path to resource.
	 * @param string $root_url  Absolute URL to resource. Empty string is accepted as an alternative
	 * in case of empty string it will be understood that resource is not public. Default: ''
	 */
	public function __construct(string $root_path, string $root_url='') {
		$this->root_path 	= \trailingslashit($root_path);
		$this->root_url 	= (!empty($root_url)) ? \trailingslashit($root_url) : '';
	}

	/**
	 * Checks if resource is accessible via URL.
	 * 
	 * @return bool In case resource is accessible via URL true, false otherwise.
	 */
	public function is_url_accessible(): bool {
		return (empty($this->root_url)) ? false: true;
	}

	/**
	 * Gets absolute path of an item in resource folder.
	 * 
	 * @param  string $resource_identifier File in given resource.
	 * @return string                      On success absolute path to file, empty string otherwise.
	 */
	public function path(string $resource_identifier): string {
		$path = $this->root_path . $resource_identifier; 

		if (!file_exists($path)) {
			return '';
		}

		return $path;
	}

	/**
	 * Gets absolute URL of an item in resource folder.
	 * 
	 * @param  string $resource_identifier File in given resource.
	 * @return string                      URL on success, empty string otherwise.
	 */
	public function url(string $resource_identifier): string {
		if ($this->path($resource_identifier) == '') {
			return '';
		}

		return ($this->root_url . $resource_identifier);
	}

}