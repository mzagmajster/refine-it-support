<?php

declare(strict_types=1);

namespace RefineIt\Support;

use RefineIt\Support\Plugin\ResourceBase;
use RefineIt\Info;


class TemplatesResource extends ResourceBase {
	
	/**
	 * Constructor.
	 * 
	 * @param string $root_path Absolute path to templates folder.
	 */
	public function __construct(string $root_path) {
		parent::__construct($root_path, '');
	}

	/**
	 * Render template.
	 * 
	 * @param  string $template_file Filename of a template.
	 * @param  array  $data          List of variables to use with the template
	 * @return void
	 */
	public function render(string $template_file, array $data): string {

		// Ensure we have real absolute paths.
		// Note: We cam use RefineIt Info here because we only need root plugin path.
		$real_root = realpath($this->root_path);
		$real_plugin_base = realpath(Info::root_plugin_path());

		// Check theme for this template.
		$i = strlen($real_plugin_base);
		$relative_path = substr($this->root_path, $i);
		$theme_path = \get_template_directory();

		// Real plugin folder name.
		$plugin_root = substr($real_plugin_base, strrpos($real_plugin_base, '/'));

		// Note here we already have froward slashes so we do not need to add them.
		$path = $theme_path . $plugin_root . $relative_path . '/' . $template_file;
		if(!file_exists($path)) {
			// Try to load default template.
			$path = $this->root_path . '/' . $template_file;
			if(!file_exists($path)) {
				return '';
			}
		}

		ob_start();
		extract($data);
		include $path;
		$content = ob_get_clean();
		return $content;
	}
}