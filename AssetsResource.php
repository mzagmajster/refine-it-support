<?php

declare(strict_types=1);

namespace RefineIt\Support;

use RefineIt\Support\Plugin\ResourceBase;

class AssetsResource extends ResourceBase {

	/**
	 * Constructor.
	 * 
	 * @param string $root_path Absolute path to folder inside assets/.
	 * @param string $root_url  Absolute URL to folder inside assets/.
	 */
	public function __construct(string $root_path, string $root_url) {
		parent::__construct($root, $root_url);
	}
}